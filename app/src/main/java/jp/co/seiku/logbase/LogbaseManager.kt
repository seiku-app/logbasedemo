package jp.co.seiku.logbase

import android.app.Activity
import android.util.Log
import net.p_lucky.logbase.DeviceId
import net.p_lucky.logbase.DeviceIdCallback
import net.p_lucky.logbase.init.LogBaseInitializer
import net.p_lucky.logbase.init.LogBaseParams
import net.p_lucky.logbase.init.LogBaseServices
import net.p_lucky.logpop.*
import net.p_lucky.logpush.CustomFieldHandler
import org.json.JSONObject

/**
 * Logbase管理マネージャー
 */
object LogbaseManager {
    // テスト配信の場合、カスタムフィールド JSON形式
    var pushJSONObject: JSONObject? = null

    private var activity: Activity? = null
    lateinit var logBaseServices: LogBaseServices
        private set

    // ディバイストークン
    private var deviceToken: String = ""

    /**
     * 初期化します。
     */
    fun initialize(activity: Activity) {
        this.activity = activity
        logBaseServices = LogBaseInitializer.initialize(activity, getLogBaseParams())
        deviceToken = logBaseServices.logPush()?.token.toString()
    }

    /**
     * パラメータを取得します。
     */
    fun getLogBaseParams(): LogBaseParams? {
        // onCreate() での初期化時に CustomFieldHandler を LogBaseParams に渡す
        val customFieldHandler: CustomFieldHandler = object : CustomFieldHandler {

            // ★★★Logbaseのテスト配信の「カスタムフィールド」を設定しないと、この関数をコールしない★★★
            override fun handleCustomField(p0: JSONObject) {
                Log.d("", "handleCustomField：[onCreate] " + p0.toString())

                pushJSONObject = p0
            }
        }

        return LogBaseParams.builder()
            .deviceIdCallback(object : DeviceIdCallback {
                // デバイス識別子が作成されたときに呼ばれます
                override fun onCreate(deviceId: DeviceId) {
                    Log.d("", "DeviceIdCallback：[onCreate] " + deviceId.token())
                    deviceToken = deviceId.token()

                }

                // デバイス識別子が変更されたときに呼ばれます
                override fun onChange(from: DeviceId, to: DeviceId) {
                    Log.d("", "DeviceIdCallback：[onChange] from: $from, to: $to")
                    deviceToken = to.token()
                }
            })
            .onGetTokenListener {
                Log.d("", "onGetTokenListener：[onCreate] " + it.toString())
            }
            .popUpEventHandler(object : PopUpEventHandler {
                // ポップアップディスプレイが表示されたときに呼ばれます
                override fun displayed(displayInfo: DisplayInfo) {
                    Log.d("", "PopUpEventHandler：[displayed] $displayInfo")
                }

                // ポップアップディスプレイ内のボタン（閉じるボタン以外）がタップされたときに呼ばれます
                override fun buttonTapped(buttonTapInfo: ButtonTapInfo) {
                    Log.d("", "PopUpEventHandler：[buttonTapped] $buttonTapInfo")
                }

                // ポップアップディスプレイ内の閉じるボタンがタップされたときに呼ばれます
                override fun closed(closeInfo: CloseInfo) {
                    Log.d("", "PopUpEventHandler：[closed] $closeInfo")
                }

                // ポップアップディスプレイが表示される直前に呼ばれます
                override fun willDisplay(willDisplayInfo: WillDisplayInfo): Boolean {
                    Log.d("", "PopUpEventHandler：[willDisplayInfo] $willDisplayInfo")
                    // false を返すとポップアップディスプレイが表示されません
                    return true
                }
            })
            .customFieldHandler(customFieldHandler)
            .build()
    }
}