package jp.co.seiku.demo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import jp.co.seiku.logbase.LogbaseManager

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        LogbaseManager.initialize(this);
    }

    @Override
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent);
        // logbas設定します。
        LogbaseManager.logBaseServices!!.logPush()!!.onNewIntent(intent);
    }

    override fun onResume() {
        super.onResume()

        LogbaseManager.logBaseServices.logEvent("testEvent01")
        LogbaseManager.logBaseServices.setTag("testTag01", "value01")

    }
}
